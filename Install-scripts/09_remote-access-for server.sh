# install xrdp
sudo apt update
sudo apt install xrdp

# check if xrdp is running
sudo systemctl status xrdp
sudo adduser xrdp ssl-cert

# add xrdp to firewall
# sudo ufw allow 3389

# von https://bytexd.com/xrdp-debian/ genommen!