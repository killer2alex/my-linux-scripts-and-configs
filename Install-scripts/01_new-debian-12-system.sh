# Debian 12 Minimal System

# give user sudo rights
# su -
# usermod -aG sudo opslocs

# Linux updaten
sudo apt update -y
sudo apt upgrade -y
sudo apt dist-upgrade -y
sudo apt autoremove -y
sudo apt autoclean -y

# sehr grundlegende Sachen hinzufügen
sudo apt -y install git curl vim aptitude

# Essential Debian Pakages
sudo aptitude -y install apt-file apt-show-versions apt-utils check-dfsg-status

# Dev-Essentials
sudo aptitude -y install make build-essential

# create important directorys
mkdir -p $HOME/bin

# Hinweis
# Grundlage für diese Sachen war der Link: https://www.dwarmstrong.org/minimal-debian/

