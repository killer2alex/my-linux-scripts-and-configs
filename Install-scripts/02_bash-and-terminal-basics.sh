# Bash und Terminal Tools + Sync

# Other ???
sudo aptitude -y install firmware-misc-nonfree gnupg htop keychain lm-sensors ncdu rsync

# Terminal-Tools
sudo aptitude -y install command-not-found shellcheck tmux tree unzip whois plocate

# Install unison and organize sync and backup
sudo aptitude -y install unison

cp -r $HOME/my-linux-scripts-and-configs/general-configs/.unison $HOME/.unison
unison sync_general_configs.prf

ln -s $HOME/my-linux-scripts-and-configs/sync_system_settings.sh $HOME/bin/sync_system_settings
(crontab -l ; echo "15,45 * * * *  unison -batch sync_general_configs.prf") | crontab -

