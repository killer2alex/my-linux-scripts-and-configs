# Tastaturlayout (Neo 2 QWERTZ)  einstellen
# =========================================

# Vorsicht! mit dem folgenden Skript wird Neo 2 (QWERTZ) als Tastatur für 
# alle Benutzer des Systems eingestellt. Das eignet sich also nur für 
# Single-Person Computer

# Neo 2 (QWERTZ) herunterladen und einstellen
# -------------------------------------------
sudo localectl --no-convert set-x11-keymap de pc105 neo_qwertz
wget "https://neo-layout.org/download/XCompose" -O ~/.XCompose

# Tastatur für alle Terminals einstellen
# --------------------------------------
sudo mkdir -p /usr/share/keymaps
wget "https://neo-layout.org/download/console.tar.xz" -O - | \
sudo tar -C /usr/share/keymaps/ -xJ

echo "KMAP=/usr/share/keymaps/neo/neoqwertz.map" | \
sudo tee -a /etc/default/keyboard
sudo setupcon

sudo update-initramfs -u -k all
sudo reboot

# HINWEISE
# ========

# Mehr Infos gibt es unter https://wiki.debian.org/Keyboard
# Da wurden auch die notwendigen Infos herausgenommen!
# Mit dem oberen Code wird das obere Tastaturlayout auch in grub verwendet.

# Nützlich ist auch der https://neo-layout.org/Einrichtung/Linux/ Link
