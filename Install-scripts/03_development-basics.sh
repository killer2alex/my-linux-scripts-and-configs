# Homebrew muss bereits installiert sein!

# Nvim
brew install nvim

# JJ
brew install jj

# Python
# brew install python
# ln -s /home/linuxbrew/.linuxbrew/bin/python3 ~/bin/python

brew install uv
uv python install
uv tool install ruff pylyzer pytest

# JavaScript
brew install npm

# Rust
brew install rust cargo

# WezTerm
curl -fsSL https://apt.fury.io/wez/gpg.key | sudo gpg --yes --dearmor -o /usr/share/keyrings/wezterm-fury.gpg
echo 'deb [signed-by=/usr/share/keyrings/wezterm-fury.gpg] https://apt.fury.io/wez/ * *' | sudo tee /etc/apt/sources.list.d/wezterm.list
sudo apt update
sudo apt install wezterm
