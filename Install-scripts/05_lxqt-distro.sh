# Basic Debian Desktop
# --------------------

# Basic X-Server
sudo apt install xorg x11-xfs-utils

# https://packages.debian.org/bookworm/task-desktop
sudo apt install xserver-xorg-input-all xserver-xorg-video-all anacron preload alsa-utils fonts-symbola
sudo apt install avahi-daemon cups iw libnss-mdns xdg-utils 

# falls Wacom-Stift
# sudo apt install xserver-xorg-input-wacom

# weitere nicht von der Seite?
sudo apt install synaptic system-config-printer

# Windowmanager and Display-manager + Stuff
# -----------------------------------------

# Displaymanager
# https://wiki.debian.org/LightDM
# brauche ich auch accountsservice und upower?
sudo apt install lightdm 
sudo dpkg-reconfigure lightdm

# deactivate?
# sudo systemctl disable lightdm.service 

sudo mkdir -p /etc/lightdm/lightdm.conf.d
sudo cp $HOME/my-linux-scripts-and-configs/additional-configs/lightdm/my-lightdm.conf /etc/lightdm/lightdm.conf.d/my-lightdm.conf

# ohne display-Manager?
cp /etc/X11/xinit/xinitrc ~/.xinitrc
# TODO 
# Hier noch den Startup reinschreiben?

# Window-Manager
# https://packages.debian.org/bookworm/openbox
# obconf-qt anstelle von obconf weil lxqt
sudo apt install openbox obconf-qt
sudo apt install menu fonts-dejavu papirus-icon-theme
# sudo 


# LXQT Install
# ------------
sudo apt install lxqt-core
sudo apt --no-install-recommends install lxqt 

# LXQT recomendet pakages
# https://packages.debian.org/sid/lxqt
sudo apt install audacious cmst gucharmap qpdfview 
# Other maybe pakages?!
# shutter