-- Import the wezterm module for changes and create config table
local wezterm = require "wezterm"
local config = wezterm.config_builder()

-- WezTerm Window settings
config.inactive_pane_hsb = {
    saturation = 0.8,
    brightness = 0.5
}
config.window_decorations = "INTEGRATED_BUTTONS | RESIZE"
config.color_scheme = "melange_dark"

-- dead keys can create problems in vim or shell hotkeys 
config.use_dead_keys = false

-- Number of lines to scroll back and use
config.scrollback_lines = 3000

config.default_cursor_style = "SteadyBar"

-- command palette
config.command_palette_font_size = 11.0
config.ui_key_cap_rendering = "WindowsSymbols"

-- prepare for export
local module = {}
module.config = config
return module
