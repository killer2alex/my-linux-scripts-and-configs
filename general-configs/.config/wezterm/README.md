# WezTerm Config

## Files for Configuration

The following files are use for the configuration. The Stuff inside is shown behind.

**wezterm.lua**
- root config file for the other config files
- microsoft windows check 
- leader definition
- Start up Decision

**terminal.lua**: 
- Basic Terminal settings (command pallete, window settings, window design)

**appearance.lua**: Basic appearence font, color and stuff.

**remap.lua**: all shortcuts and hotkeys
