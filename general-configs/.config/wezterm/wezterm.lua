-- Import the wezterm module for changes and create config table
local wezterm = require "wezterm"
local config = wezterm.config_builder()

-- mux manages the multiplexer layer, which is responsible for everything 
-- related to panes, tabs, windows, and workspaces, but not the terminal itself.
local mux = wezterm.mux
 
-- IMPORTANT: Sets WSL2 Debian as the defualt when opening Wezterm
if wezterm.target_triple == "x86_64-pc-windows-msvc" then
   config.default_domain = "WSL:Debian"
end

-- Set up Start up to maximize window
wezterm.on("gui-startup", function()
    local tab, pane, window = mux.spawn_window({})
    window:gui_window():maximize()
end)

-- Set Leader to STRG+A start shorcuts from
-- Can be used with `mods = "LEADER"`
config.leader = {key = "ö", mods = "ALT", timeout_milliseconds = 2000}


-- Import my own font module in the same folder
local font = require "font"
local terminal = require "terminal"

-- Add configs from externale files
for k,v in pairs(terminal.config) do config[k] = v end
for k,v in pairs(font.config) do config[k] = v end

local remap = require "remap"
config.keys = {}
for k,v in pairs(remap.config.keys) do config.keys[k] = v end


-- Returns our config to be evaluated. Must always be at the bottom 
return config

