-- Import the wezterm module for changes and create config table
local wezterm = require "wezterm"
local config = wezterm.config_builder()

-- helps us bind keys to actions
local act = wezterm.action

config.keys = {
    -- Window

    -- Panes
    {key="f", mods="LEADER", action=act.TogglePaneZoomState},
    {key="p", mods="LEADER", action=act.PaneSelect {mode="Activate"}},
    {key="w", mods="LEADER", action=act.CloseCurrentPane {confirm=true}},

    {key="j", mods="LEADER", action=act.ActivatePaneDirection("Down")},
    {key="k", mods="LEADER", action=act.ActivatePaneDirection("Up")},
    {key="h", mods="LEADER", action=act.ActivatePaneDirection("Left")},
    {key="l", mods="LEADER", action=act.ActivatePaneDirection("Right")},

    {key="-", mods="LEADER", action=act.SplitVertical{domain="CurrentPaneDomain"}},
    {key="|", mods="LEADER", action=act.SplitHorizontal{domain="CurrentPaneDomain"}},

    -- Tabs
    {key="t", mods="CTRL", action=act.SpawnTab "CurrentPaneDomain"},
    {key="w", mods="CTRL", action=act.CloseCurrentTab { confirm = true }},
    {key="Tab", mods="LEADER|CTRL", action=act.ActivateTabRelative(-1)},
    {key="Tab", mods="LEADER|CTRL|SHIFT", action=act.ActivateTabRelative(-1)},
    {key ='1', mods="LEADER|ALT", action=act.ActivateTab(0)},
    {key ='2', mods="LEADER|ALT", action=act.ActivateTab(1)},
    {key ='3', mods="LEADER|ALT", action=act.ActivateTab(2)},
    {key ='4', mods="LEADER|ALT", action=act.ActivateTab(3)},
    {key ='5', mods="LEADER|ALT", action=act.ActivateTab(4)},
    -- Terminal (Command pallate, )
    {key="p", mods="ALT", action=act.ActivateCommandPalette},

    -- use STRG + Backspace to delete words backwards (normaly STRG + w)
    {key="Backspace", mods="CTRL", action=act.SendKey({key="w",mods="CTRL"})},
}

-- prepare for export
local module = {}
module.config = config
return module

