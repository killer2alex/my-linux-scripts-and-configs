-- Import wezterm module
local wezterm = require "wezterm"


-- Define a empty lua table to hold our module's functions
local config = wezterm.config_builder()

-- disable ligatures in most fonts
config.harfbuzz_features = { "calt=0", "clig=0", "liga=0" }
config.font_size = 11
config.font = wezterm.font("JetBrains Mono")
config.line_height = 1.1

-- prepare for export
local module = {}
module.config = config
return module
