return {
    'nvim-telescope/telescope.nvim', tag = '0.1.8',  
-- or                              , branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim' },
    lazy = true,
    keys = function()
        local builtin = require('telescope.builtin')
        return {
            {"<A-o>", builtin.find_files, desc = "Search for files"},
            {"<leader>fg", builtin.live_grep, desc = "Search for text in files"},
            {"<leader>fb", builtin.buffers, desc = "Search through buffers"},
            {"<leader>fh", builtin.help_tags, desc = "Search through help tags"},
        }
    end,
}
