
-- muss vor lazy kommen, damit der leader geladen wird
require("config.remap")

-- require("config.vscode")

-- Use lazy.nvim for plugin management
require("config.lazy")
-- Muss nach Lazy kommen, damit die Farbe geladen wird
require("config.set_editor")
require("config.telescope")
-- WSL
if vim.fn.has('wsl') == 2 then
    require("config.set_wsl")
end
