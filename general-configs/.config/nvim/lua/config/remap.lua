-- map leader to <Space>
    vim.keymap.set("n", " ", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.keymap.set("n", "<Leader>h", function() print("Leader working!") end)

-- replace esc with jk 
vim.keymap.set({"i", "v", "x", "o", "s" }, "jk", "<esc>")
vim.keymap.set({"i", "v", "x", "o", "s" }, "JK", "<esc>")
vim.keymap.set("c", "jk", "<C-C>")

-- go to first Word, not first Line with Neo2
vim.keymap.set({"i"}, "<Home>", "<C-o>^")
vim.keymap.set({"n"}, "<Home>", "^")

-- indent with Tab and unindent with Shift + Tab
vim.keymap.set({"n"}, "<S-Tab>", "<<")
vim.keymap.set({"n"}, "<Tab>", ">>")
vim.keymap.set ({"i"}, "<S-Tab>","<C-D>")
vim.keymap.set ({"i"}, "<Tab>","<C-T>")

-- Telescope
--local builtin = require('telescope.builtin')
--vim.keymap.set({"n","i","v"}, '<A-o>', builtin.find_files, { desc = 'Telescope find files' })
--vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Telescope live grep' })
--vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
--vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })
