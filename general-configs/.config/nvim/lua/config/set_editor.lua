
-- Basic Editor UI
vim.opt.number = true           -- show line numbers
vim.opt.relativenumber = true   -- show relative linenumbers
vim.opt.colorcolumn = "80"      -- show Line for 80 chars
vim.opt.cursorline = true       -- highlight the current line in editor
vim.opt.termguicolors = true
vim.cmd.colorscheme 'melange'

-- Indenting
vim.opt.tabstop = 4	            -- Basic Tabstop
vim.opt.softtabstop = 4	        -- Additional Setting for Tabstop
vim.opt.shiftwidth = 4          -- ???
vim.opt.expandtab = true        -- converts tabs to spaces
vim.opt.autoindent = true       -- autmaticly indent to level of last line

-- Working with the rest of the system
vim.opt.clipboard = "unnamedplus"

