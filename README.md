# My Linux Scripts and configs

## Setup and Settings

Dieses Repo muss im $HOME-Directory des Linux-Systems liegen.

Zur einfachen Syncronisation wird `unison` gemeinsam mit dem
`commit_system_settings.sh` verwendet, welche beide mittels
der Install Skripte installiert werden. Der Befehl `commit_system_settings`
steht dann zur einfachen syncronisation zur Verfügung

## Structure

Insgesamt werden alle configs

## Setup and Settings

Hier werden grundlegende Dateien für meine Verwendung mit Linux bereitgestellt.
Dafür das Repository mittels HTTPS Clonen und dann denn Inhalt nach $HOME verschieben.

Vorher wird noch Git benötigt => `sudo apt install git`
Und es wird empfohlen die URL zu shorten (bsp. tinurl) zu linux-basic-starter
(https://gist.github.com/089966a9eb8c7e30d23741b887258902.git)

WICHTIG!:
Die Ordner `general-configs`, `lxqt-configs`, `kde-configs` werden vom Home-dir aus gesehen.
### Downloaden und Verschieben

Vorsicht! Überschreibt eine Dateien im $HOME.

```bash
cd ~
git clone https://tinyurl.com/linux-basic-starter 
cd linux-basic-starter
cp .b* ~/
cp .profile ~/
# `~/.bash_wsl2_commands` entfernen wenn notwendig
git remote set-url origin https://gist.github.com/089966a9eb8c7e30d23741b887258902.git
```

### System updaten
```bash
sudo apt update
sudo apt upgrade
sudo apt dist-upgrade
sudo apt autoremove
sudo apt autoclean
# Firmware updates ziehen
sudo fwupdmgr get-devices
sudo fwupdmgr get-updates
sudo fwupdmgr update
flatpak update
sudo reboot now
```

### Browser besorgen

Es wird dafür [Brave](https://brave.com/linux/#linux) verwendet

```bash
sudo apt install apt-transport-https curl

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update

sudo apt install brave-browser
```

### Den `ssh-agent` die Schlüssel zeigen

Dafür wird der folgende Befehl ausgeführt:
`cd ~/.ssh ssh-add *`
