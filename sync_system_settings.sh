unison sync_general_configs.prf
unison sync_kde_configs.prf
cd $HOME/my-linux-scripts-and-configs
git status

echo "Automatische Speicherung durchführen?(y/n)"
read answer
if [ $answer = "y" ] || [ $answer = "j" ] ; then
  echo $(git commit -am "Automatische Speicherung")
fi

echo "Repo pushen?(y/n)"
read answer2
if [ $answer2 = "y" ] || [ $answer2 = "j" ] ; then
  echo $(git push)
fi

echo "Vorgang beendet"
